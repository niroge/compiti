/* secondo.cc : secondo esercizio in C++
 * Calcola l'area del trapezio nota la base e l'altezza
 */

#include "robert_cpp.h"

int main(void)
{
        /* imposta il titolo e i colori del programma se il SO è Windows */
#       ifdef nt
        system("TITLE Programma inutile sull'area del trapezio");
        system("color f0");
#       endif
        
        /* variabili di I */
        double base, altezza;

        /* variabili di verifica */
        int status;

        printf("base, altezza: ");

        status = scanf("%lf %lf", &base, &altezza);

        se (status != 2 || (base <= 0 || altezza <= 0))
                esci_errore("[X] ERRORE! Input invalido...", 0, 1);

        printf("Area: %lf %s", base * altezza / 2, NEWLINE);

        pausa();

        exit(ESCI_OK);
}
