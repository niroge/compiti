/* primo.cc : primo esercizio in C++ 
 * Risolvi la formula (x + y + z) ^ 2
 */

#include "robert_cpp.h"

int main(void)
{
        /* imposta il titolo e i colori del programma se il SO è Windows */
#       ifdef nt
        system("TITLE (x + y + z)(x + y + z)");
        system("color f0");
#       endif

        /* variabili di input */
        double x, y, z;

        printf("x, y, z: ");

        esci_errore("[X] ERRORE! Input invalido", scanf("%lf %lf %lf", &x, &y, &z), 3);

        printf("Risultato: %lf %s", (x + y + z) * (x + y + z), NEWLINE);

        pausa();

        exit(ESCI_OK);
}
