#include <cstdio>
#include <cstdlib>

#define ESCI_OK 0
#define ESCI_ERRORE 1

#define se if
#define altrimenti else

#define non_e !=

#ifdef unix
#define NEWLINE "\n"
#else
#define NEWLINE "\r\n"
#endif

void esci_errore(const char *msg, int stato, int giusto)
{
  se (stato non_e giusto) {
    fprintf(stderr, "%s", msg);
    puts("");
    exit(ESCI_ERRORE);
  }
}

void pausa(void)
{
#       if nt
        system("PAUSE");
#       endif
}
