CC=clang
CXX=clang++
CFLAGS=-O3 -march=native -Wall -pipe -flto -fomit-frame-pointer
CXXFLAGS=${CFLAGS}

compile: compile_c compile_cpp
	@echo ":: Compilazione di tutti i programmi completata!"

cleanup:
	@echo ":: Rimozione dei file eseguibili in corso"
	rm *.out
	@echo ":: Pulizia completata!"

compile_c: primo.c secondo.c terzo.c quarto.c
	@echo "Compilazione dei programmi in C..."
	@echo "=================================="
	@echo ""
	${CC} ${CFLAGS} primo.c -o primo.out
	${CC} ${CFLAGS} secondo.c -o secondo.out
	${CC} ${CFLAGS} terzo.c -o terzo.out
	${CC} ${CFLAGS} quarto.c -o quarto.out
	@echo ":: Compilazione dei programmi in C completata"
	@echo ""

compile_cpp: primo.cc secondo.cc terzo.cc quarto.cc
	@echo "Compilazione dei programmi in C++..."
	@echo "===================================="
	@echo ""
	${CXX} ${CXXFLAGS} primo.cc -o primo_cpp.out
	${CXX} ${CXXFLAGS} secondo.cc -o secondo_cpp.out
	${CXX} ${CXXFLAGS} terzo.cc -o terzo_cpp.out
	${CXX} ${CXXFLAGS} quarto.cc -o quarto_cpp.out
	@echo ":: Compilazione dei programmi in C++ completata"
	@echo ""

run: run_all

run_all: compile
	@echo "ESECUZIONE DI OGNI PROGRAMMA"
	@echo "============================"
	@echo ""
	@echo ":: Esecuzione di primo.out..."
	./primo.out
	@echo ">> Fine esecuzione"
	@echo ""
	@echo ":: Esecuzione di primo_cpp.out..."
	./primo_cpp.out
	@echo ">> Fine esecuzione"
	@echo ""
	@echo ":: Esecuzione di secondo.out..."
	./secondo.out
	@echo ">> Fine esecuzione"
	@echo ""
	@echo ":: Esecuzione di secondo_cpp.out..."
	./secondo_cpp.out
	@echo ">> Fine esecuzione"
	@echo ""
	@echo ":: Esecuzione di terzo.out..."
	./terzo.out
	@echo ">> Fine esecuzione"
	@echo ""
	@echo ":: Esecuzione di terzo_cpp.out..."
	./terzo_cpp.out
	@echo ">> Fine esecuzione"
	@echo ""
	@echo ":: Esecuzione di quarto.out..."
	./quarto.out
	@echo ">> Fine esecuzione"
	@echo ""
	@echo ":: Esecuzione di quarto_cpp.out..."
	./quarto_cpp.out
	@echo ">> Fine esecuzione"

diff: primo.c primo.cc secondo.c secondo.cc terzo.c terzo.cc quarto.c quarto.cc
	@echo "primo.c vs primo.cc"
	@echo "==================================================================="
	@-diff --color primo.c primo.cc
	@echo "==================================================================="
	@echo ""
	@echo "secondo.c vs secondo.cc"
	@echo "==================================================================="
	@-diff --color secondo.c secondo.cc
	@echo "==================================================================="
	@echo ""
	@echo "terzo.c vs terzo.cc"
	@echo "==================================================================="
	@-diff --color terzo.c terzo.cc
	@echo "==================================================================="
	@echo
	@echo "quarto.c vs quarto.cc"
	@echo "==================================================================="
	@-diff --color quarto.c quarto.cc
	@echo "==================================================================="
	@echo ""
