/* terzo.c : terzo esercizio in C++
 * Dati base e altezza di un triangolo, calcolare l'area se
 * sono entrambi positivi, altrimenti stampa un messaggio di
 * errore.
 */

#include "robert_cpp.h"

int main(void)
{
        /* imposta il titolo e i colori del programma se il SO è Windows */
#       ifdef nt
        system("TITLE Programma inutile sull'area del rettangolo");
        system("color f0");
#       endif

        /* variabili di input */
        double base, altezza;

        /* variabili di lavoro */
        int status;

        printf("Base, altezza: ");

        status = scanf("%lf %lf", &base, &altezza);

        se (status != 2 || (base <= 0 || altezza <= 0))
                esci_errore("[X] ERRORE! Dati di input invalidi", 0, 1);

        printf("Area: %lf %s", base * altezza / 2, NEWLINE);

        exit(ESCI_OK);
}
