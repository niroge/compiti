/* quarto.c : quarto esercizio in C++ 
 * Data l'equazione ax + b = 0, determinare 
 * il valore di x dati in input a e b
 */

#include "robert_cpp.h"

int main(void)
{
#       ifdef nt
        system("TITLE ax + b = 0");
        system("color f0");
#       endif
        /* variabili di input */
        double a, b;
        
        printf("a, b: ");
        esci_errore("[X] ERRORE! Input invalido...", scanf("%lf %lf", &a, &b), 2);

        printf("X -> %lf %s", (-b) / a, NEWLINE);

        pausa();
        
        exit(ESCI_OK);
}
